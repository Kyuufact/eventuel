<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>L'Éventuel, exploration dans le futur des formes typographiques.</title>
  <link rel="stylesheet" href="../css/190204_style.css">
  <script src="../js/src/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="../js/controls.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.15.0/lazyload.min.js"></script>
  <script src="../js/src/paper-full.min.js"></script>
  <script type="text/paperscript" canvas="canvas" src="../js/paper_script.js"></script>
</head>
<body>
  <?php require("../core/toolkit.php"); ?>
  <div id="popEntree">
  <div id="grosTitre">L'Éventuel</div><br><br><br>
  Participez à la création d’un caractère typographique, le Villa Éventuel,<br> en imaginant les prochaines étapes de l’évolution des formes de notre alphabet. Sélectionnez des lettres dans la liste et dessinez avec le curseur de votre souris.
  <br><br>
  <span>Commencer à explorer l'interface</span>  ou  <span id="randomlettre">Démarrer sur une lettre au hasard</span>
  <span><a style="color:inherit; text-decoration:none" href="https://www.villagillet.net/contact">S'inscrire à la newsletter</a></span><br>
  <div id="langue"><a href="../en">Switch to english interface</a></div>
  </div>

<main>
<section id='pageDeTitre'>
  <div id="container">
    <div id="grandTitre">L'Éventuel</div>
    <div id="petitTitre"><span>Exploration dans le futur des formes typographiques</span></div>
    <div id="temps">
      <div id="sommaireLettre">Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz</div>
    </div>
    <div id="swDessin">
      <span>Dessins</span>
    </div>
    <div id="swEcrire">
      <span>Commentaires</span>
    </div>
  </div>
</section>


<section id="dessin">
  <div id="innerDessin">
    <input id="name" type="text" placeholder="Entrez votre nom ici" class="interface i1 i2">
    <div class="interface i1" id="lettreTrans">W</div>
    <div class="interface i1" id="introLettreTrans">Dessinez ici avec le curseur de votre souris.</div>
    <canvas id="canvas" class="interface i1" resize></canvas>
    <button data-letter="a" data-type="letter" class="download-svg interface i1 ">Envoyer</button>
    <button data-letter="a" data-type="comz" class="download-svg interface i2 com">Envoyer</button>

    <button id="erase" class="interface i1">Effacer le dernier trait</button>

    <div id="explication" class="interface i0">
      <p> Il y a un an, la <a href="http://www.villagillet.net">Villa Gillet</a>, centre international pluridisciplinaire, introduisait le <i>Villa</i>, son nouveau caractère typographique, réalisé par <a href="http://le-combo.fr">le Combo</a> et  composé de deux styles&nbsp;: le <span>Régulier</span>, dont les formes se fondent sur les archétypes des grandes familles typographiques, et l'<span><i>Irrégulier</i></span>, italique associée qui contrairement à son compagnon, va chercher ses courbes dans des modèles atypiques n'ayant jamais réussi à faire standard. Tous deux proposent donc un regard sur l’histoire de cette discipline.</p>
      <p> Cette année, vous êtes invités à participer à la conception d'une troisième déclinaison du caractère : une version grasse baptisée l'<span>Éventuel</span>, dont le dessin sera témoin d’explorations des formes futures de notre alphabet. Car, si à l’échelle d’une vie notre système d’écriture nous semble immuable, il est en fait le fruit d’une transformation constante depuis plusieurs milliers d’années. Ainsi, il y a fort à parier que d’ici mille ans, nos lettres auront subi quelques mutations&nbsp;!</p>
      <p> À vous maintenant d’imaginer les potentielles prochaines étapes de cette évolution, en intervenant sur les lettres de votre choix. L’ensemble des propositions récoltées servira de base à un travail de dessin typographique, aboutissant à la création du <span>Villa Éventuel</span>.</p>
    </div>

    <textarea placeholder="Écrivez ici..." name="name" rows="8" cols="80" id="commentaire" class="interface i2" required>


    </textarea>
  </div>
</section>

<section id="propositions">

  <div id="listecommentaire" class="interface i2">
    <?php $comz = json_decode(file_get_contents(dirname(dirname(__FILE__))."/data/writting/text.json"), true) ?>
    <?php foreach ($comz as $key => $com): ?>
      <?php $info = json_decode($com, true); ?>
      <div class="commentaire">
        <div class="informations">
          <span class="pseudo"><?php echo $info["nom"]; ?></span>
        le <span class="date"><?php echo $info["date"] ?></span>
        </div>
        <div class="message">
          <?php echo $info["com"] ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>



  <ul class="interface i3">

    <?php foreach ($lettres as $key => $value): ?>
      <?php  $timeline_dir = "../data/timeline/*"?>
      <?php  $drawing_dir = "../data/drawing/*"?>
      <?php $counter = 0; ?>
      <?php $time_array = [] ?>
      <li class='comp'>
        <div class='nomLettre'><?php echo $value ?></div>
        <div style="border-right:none;" class='petitDessins'>
          <div class='innerPetitDessins'>
            <?php foreach (glob($drawing_dir) as $filename): ?>
              <?php
                $info = explode("_",$filename);
                $c = substr(strrchr($info[0], "/"), 1)
              ?>
              <?php if ($c == $value ): ?>
                <img src="<?php echo $filename ?>" alt="">
              <?php endif; ?>
            <?php endforeach; ?>
          </div>
        </div>
        <div class='occurencesLettre'>

        </div>
        <div class='bouttonDessin' data-lettre="<?php echo $value ?>">Dessiner</div>
      </li>
      <li class='infoscomp'>
        <div class='titreTimeline'>Évolution des dessins de lettres:</div>


        <div class='timeline'>
        <?php foreach (glob($timeline_dir) as $filename): ?>


          <?php
            $info = explode("_",$filename);
            $c = $info[1];
          ?>
          <?php if ($c == $value ): ?>

            <?php $era = explode(".", $info[2]); ?>
            <?php array_push($time_array, array(
              "file"=>$filename,
              "era" => $era[1],
              "year" => $era[0]
            )) ?>

          <?php endif; ?>

        <?php endforeach; ?>
        <?php
        usort($time_array, function($a, $b) {
          return $a['year'] <=> $b['year'];
        });
        ?>
        <?php foreach ($time_array as $i_letter): ?>
          <div class='lettreTime'>
            <img class="lazy" data-src='<?php echo $i_letter["file"]; ?>'/>
            <div class='periodeLettre'><?php echo $i_letter["era"]; ?><br> <?php echo $i_letter["year"]; ?></div>
          </div>
          <div class='flecheTimeline'>></div>
        <?php endforeach; ?>
        </div>

        <?php foreach (glob($drawing_dir) as $filename): ?>
          <?php
            $info = explode("_",$filename);
            $c = substr(strrchr($info[0], "/"), 1)
          ?>
          <?php if ($c == $value ): ?>
            <?php $counter ++ ?>
          <?php endif; ?>
        <?php endforeach; ?>
        <div class='nbDessin'><?php echo $counter ?> dessins <br> déjà proposés</div>
          <div class='dessinPropose'>
            <div class='innerDessinPropose'>
              <?php foreach (glob($drawing_dir) as $filename): ?>
                <?php
                  $info = explode("_",$filename);
                  $c = substr(strrchr($info[0], "/"), 1)
                ?>
                <?php if ($c == $value ): ?>
                  <img class="lazy" data-src="<?php echo $filename ?>" alt="">
                <?php endif; ?>
              <?php endforeach; ?>
             </div>
           </div>
       </li>
    <?php endforeach; ?>
  </ul>
</section>
</main>
<script type="text/javascript">
  //lazyload
  var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy"
  });
</script>
</body>
</html>
